# Use uma imagem base com Node.js
FROM node:lts

# Defina o diretório de trabalho no contêiner
WORKDIR /usr/src/app

# Copie os arquivos do projeto para o contêiner
COPY . .

# Instale as dependências
RUN npm install

# Construa o aplicativo Vue.js
RUN npm run build

# Exponha a porta 80 (ou a porta que você configurou no seu aplicativo Vue.js)
EXPOSE 80

# Comando para iniciar o servidor quando o contêiner for iniciado
CMD ["npm", "run", "dev"]
