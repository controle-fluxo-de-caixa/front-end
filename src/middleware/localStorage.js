// Função para definir um item no LocalStorage
export function setLocalStorageItem(key, value) {
    if (typeof value === 'object') {
        value = JSON.stringify(value);
    }
    localStorage.setItem(key, value);
}

// Função para obter um item do LocalStorage
export function getLocalStorageItem(key) {
    const value = localStorage.getItem(key);
    try {
        return JSON.parse(value);
    } catch (error) {
        return value;
    }
}
