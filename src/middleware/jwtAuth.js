import jwtDecode from "vue-jwt-decode";

export function isTokenExpired() {
    const token = localStorage.getItem('jwt_token');
    if (!token) {
        localStorage.clear();
        return true; // Não há token, considerar como expirado
    }
    const payload = jwtDecode.decode(token);
    if (payload.exp && payload.exp * 1000 < Date.now()) {
        localStorage.clear();
        return true; // Token expirou
    }
    return false;
}
