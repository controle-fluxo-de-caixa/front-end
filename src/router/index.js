import { createRouter, createWebHistory } from "vue-router";
import { isTokenExpired } from "@/middleware/jwtAuth";
import Login from "@/views/auth/LoginView.vue";
import UserPerfil from "@/views/auth/UserPerfil.vue";
import CreateUser from "@/views/CreateUserView.vue";
import FinancialMovement from "@/views/FinancialMovementView.vue";
import MonthlyPayments from "@/views/payments/MonthlyPaymentsView.vue";
import MonthlyPaymentsUser from "@/views/payments/MonthlyPaymentsUserView.vue";
import ForgotPassword from "@/views/auth/ForgotPasswordView.vue";
import ResetPassword from "@/views/auth/ResetPasswordView.vue";
import Notification from "@/views/NotificationsView.vue";
import Competition from "@/views/competition/CompetitionView.vue";
import Dashboard from "@/views/dashboard/DashboardView.vue";
import Class from "@/views/ClassView.vue";
import Calendar from "@/views/Calendar.vue";
import NotFound from "@/views/NotFoundView.vue";

const routes = [
  {
    path: "/",
    component: Login,
  },
  {
    path: "/perfil",
    component: UserPerfil,
  },
  {
    path: "/criar_usuario",
    name: "CreateUser",
    component: CreateUser,
  },
  {
    path: "/esqueci_senha",
    name: "ForgotPassword",
    component: ForgotPassword,
  },
  {
    path: "/reset_password",
    name: "ResetPassword",
    component: ResetPassword,
  },
  {
    path: "/movimentacao",
    name: "FinancialMovement",
    component: FinancialMovement,
  },
  {
    path: "/mensalidade",
    name: "MonthlyPayments",
    component: MonthlyPayments,
  },
  {
    path: "/mensalidadeAluno",
    name: "MonthlyPaymentsUser",
    component: MonthlyPaymentsUser,
  },
  {
    path: "/aulas",
    name: "Class",
    component: Class,
  },
  {
    path: "/notificacoes",
    name: "Notifications",
    component: Notification,
  },
  {
    path: "/competicoes",
    name: "Competition",
    component: Competition,
  },
  {
    path: "/dashboard",
    name: "Dashboard",
    component: Dashboard,
  },
  {
    path: "/calendario",
    name: "Calendar",
    component: Calendar,
  },
  {
    path: "/:catchAll(.*)",
    name: "NotFound",
    component: NotFound,
  },
];
const router = createRouter({
  history: createWebHistory(),
  routes,
});

router.beforeEach((to, from, next) => {
  const rotasExcluidas = ["/", "/esqueci_senha", "/reset_password"];
  if (!rotasExcluidas.includes(to.path)) {
    if (isTokenExpired()) {
      if (to.path !== "/") {
        next("/");
        return; // Evita chamadas adicionais para next
      }
    }
  }
  next();
});

export default router;
