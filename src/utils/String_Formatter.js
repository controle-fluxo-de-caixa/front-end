export function moneyToFloat(value) {
  let valueSplit = value.split("R$");
  let valueFormatted = valueSplit[1].replace(".", "");
  return parseFloat(valueFormatted.replace(",", "."));
}

export function stringToInputDate(dataString) {
  const dataSplit = dataString.split("/");
  const year = dataSplit[2];
  const mounth = dataSplit[1].padStart(2, "0");
  const day = dataSplit[0].padStart(2, "0");
  return `${year}-${mounth}-${day}`;
}
export function formatDate(isoDate) {
  const date = new Date(isoDate);
  const day = date.getUTCDate().toString().padStart(2, "0");
  const month = (date.getUTCMonth() + 1).toString().padStart(2, "0");
  const year = date.getUTCFullYear();
  return `${day}/${month}/${year}`;
}

export function numberToMonth(value) {
    const mounth = [
        'JAN', 'FEV', 'MAR', 'ABR', 'MAI', 'JUN',
        'JUL', 'AGO', 'SET', 'OUT', 'NOV', 'DEZ'
    ];

    if (value >= 1 && value <= 12) {
        return mounth[value - 1];
    } else {
        return 'Desconhecido';
    }

}

export function formatMoeda(numero) {
    const negativo = numero < 0;
    numero = Math.abs(numero);
    const numeroFormatado = numero.toFixed(2);
    // Adiciona o prefixo "R$ " e um sinal de "-" se for negativo
    const moedaFormatada = "R$ " + (negativo ? "- " : "") + numeroFormatado;

    return moedaFormatada;
}

